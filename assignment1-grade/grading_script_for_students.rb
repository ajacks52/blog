`mkdir ../tests_save_temp`

`cp -R ../spec ../tests_save_temp`

`rm -R ../spec`

`cp ../Gemfile ../tests_save_temp/Gemfile`

folder_origin = File.basename(Dir.getwd)

if(`cd ..;
   echo "group :test, :development do" >> Gemfile;
   echo "gem 'rspec-rails'" >> Gemfile;
   echo "gem 'capybara'" >> Gemfile;
   echo "gem 'shoulda-matchers'" >> Gemfile;
   echo "gem 'validation_reflection'" >> Gemfile;
   echo "end" >> Gemfile;
   bundle install;
   rails g rspec:install;
   mkdir spec/features`; $?.exitstatus == 0) then
    
    
    `cp -R ./Tests_to_add/* ../spec;
    cd ..;rake db:migrate RAILS_ENV=test;
    rspec;
    rspec --format progress --out ./#{folder_origin}/results.txt`;
    first_line=File.open('./results.txt', &:readline)
    passes=first_line.count '.'
    fails=first_line.count 'F'
    total=passes+fails
    grade=100.0*passes/total
    puts '*******************************************************************'
    puts 'This script will perform CRUD based tests on your web application'
    puts '*******************************************************************'
    puts '**********************'
    puts '**** grade: '+grade.round(2).to_s+' ****'
    puts '**********************'
    puts '*******************************************************************'
    puts 'View the test results for debugging at '+folder_origin+'/results.txt'
    puts '*******************************************************************'
    
    
    
    else
    
    puts '*********************'
    puts '**** Build Error ****'
    puts '*********************'
    
end

`rm ../Gemfile`
`cp ../tests_save_temp/Gemfile ..`
`rm -R ../spec`
`cp -R ../tests_save_temp/spec ..`
`rm -R ../tests_save_temp`






